
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author javier
 */
public class Principal extends javax.swing.JFrame {

    int contador;
    /**
     * Creates new form Principal
     */
    int[][] matriz1 = new int[][]{
        {0, 1, 2, 1, 1, 1},
        {0, 0, 0, 0, 0, 0},
        {1, 0, 1, 0, 0, 0},
        {2, 0, 2, 0, 1, 0},
        {0, 1, 1, 0, 1, 0},
        {0, 1, 1, 1, 0, 0}
    };

    int[][] matriz2 = new int[][]{
        {0, 1, 0, 1, 0, 0},
        {0, 0, 0, 1, 0, 0},
        {0, 1, 0, 0, 0, 0},
        {1, 1, 0, 2, 1, 0},
        {0, 0, 0, 2, 1, 0},
        {0, 1, 1, 1, 0, 0}
    };
    int[][] matriz3 = new int[][]{
        {0, 1, 0, 1, 0, 0},
        {0, 0, 0, 1, 0, 0},
        {0, 1, 1, 0, 0, 0},
        {0, 1, 1, 1, 1, 0},
        {0, 1, 0, 0, 0, 0},
        {0, 0, 0, 1, 0, 0}
    };
    int salida_c[] = {3, 5, 4};
    int salida_f[] = {3, 1, 2};
    Object[] niveles = new Object[]{matriz1, matriz2, matriz3};

    public Principal() {
        initComponents();
        contador = 0;
        escenario1.setFocusable(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        escenario1 = new Escenario();
        arriba = new javax.swing.JButton();
        izquierda = new javax.swing.JButton();
        derecha = new javax.swing.JButton();
        abajo = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        escenario1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                escenario1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout escenario1Layout = new javax.swing.GroupLayout(escenario1);
        escenario1.setLayout(escenario1Layout);
        escenario1Layout.setHorizontalGroup(
            escenario1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 248, Short.MAX_VALUE)
        );
        escenario1Layout.setVerticalGroup(
            escenario1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 246, Short.MAX_VALUE)
        );

        arriba.setText("^");
        arriba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arribaActionPerformed(evt);
            }
        });

        izquierda.setText("<");
        izquierda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                izquierdaActionPerformed(evt);
            }
        });

        derecha.setText(">");
        derecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                derechaActionPerformed(evt);
            }
        });

        abajo.setText("v");
        abajo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                abajoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(escenario1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(izquierda)
                        .addGap(22, 22, 22))
                    .addComponent(arriba)
                    .addComponent(abajo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(derecha)
                .addContainerGap(186, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(escenario1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(arriba)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(derecha)
                    .addComponent(izquierda))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(abajo)
                .addGap(69, 69, 69))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void arribaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arribaActionPerformed
        escenario1.arriba();
        siguienteNivel();
        
    }//GEN-LAST:event_arribaActionPerformed

    private void izquierdaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_izquierdaActionPerformed
        escenario1.izquierda();
        siguienteNivel();
    }//GEN-LAST:event_izquierdaActionPerformed

    private void derechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_derechaActionPerformed
        escenario1.derecha();
        siguienteNivel();
    }//GEN-LAST:event_derechaActionPerformed

    private void abajoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_abajoActionPerformed
        escenario1.abajo();
        siguienteNivel();
    }//GEN-LAST:event_abajoActionPerformed

    private void escenario1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_escenario1KeyPressed
        int s = evt.getKeyCode();
        if (s == 40) {
            escenario1.abajo();
        }
        if (s == 38) {
            escenario1.arriba();
        }
        if (s == 39) {
            escenario1.derecha();
        }
        if (s == 37) {
            escenario1.izquierda();
        }
        siguienteNivel();
    }//GEN-LAST:event_escenario1KeyPressed
    public void siguienteNivel() {
        if (escenario1.resuelto) {
            if (contador < niveles.length) {
                contador++;
                int[][] m = (int[][]) niveles[contador - 1];
                int c = salida_c[contador - 1];
                int f = salida_f[contador - 1];
                JOptionPane.showMessageDialog(rootPane, "nuevo nivel");
                escenario1.nuevoNivel(m, 0, 0, c, f);
                this.repaint();
                escenario1.setFocusable(true);
            } else {
                JOptionPane.showMessageDialog(rootPane, "Has pasado todos los niveles");
            }
        }
        izquierda.setFocusable(false);
        derecha.setFocusable(false);
        arriba.setFocusable(false);
        abajo.setFocusable(false);
        escenario1.setFocusable(true);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton abajo;
    private javax.swing.JButton arriba;
    private javax.swing.JButton derecha;
    private Escenario escenario1;
    private javax.swing.JButton izquierda;
    // End of variables declaration//GEN-END:variables
}
