
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author javier
 */
public class Escenario extends JPanel {
    int j;
    int i;
    int aux;
    int matriz[][];
    int fila;
    int columna;
    int fila_salida;
    int columna_salida;
    boolean resuelto;
    BufferedImage personaje, puerta_salida, muro, flama;
    URL pers = getClass().getResource("imagenes/npc_boy.png");
    URL dor = getClass().getResource("imagenes/doors.png");
    URL mu = getClass().getResource("imagenes/pared.png");
    URL fla = getClass().getResource("imagenes/flami.gif");

    public Escenario() {
        matriz = new int[][]{
            {0, 1, 2, 1, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 0, 0},
            {0, 1, 1, 0, 1, 0},
            {0, 1, 1, 0, 1, 0},
            {0, 1, 1, 1, 0, 0}
        };
        fila = 0;
        columna = 0;
        fila_salida = 1;
        columna_salida = 3;
        resuelto = false;
        matriz[columna_salida][fila_salida] = 0;
        try {
            personaje = ImageIO.read(pers);
            puerta_salida = ImageIO.read(dor);
            muro = ImageIO.read(mu);
            flama = ImageIO.read(fla);
            //repaint();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "no se cargo imagen por:" + e.getMessage());
        }
        Random r = new Random();
        j = r.nextInt(matriz.length);
        r = new Random();
        i = r.nextInt(matriz.length);
        aux = matriz[i][j];
        
    }

    public void nuevoNivel(int m[][], int f, int c, int sf, int sc) {
        matriz = m;
        fila = f;
        columna = c;
        fila_salida = sf;
        columna_salida = sc;
        resuelto = false;
        matriz[columna_salida][fila_salida] = 0;
        repaint();
        this.setFocusable(true);
    }

    public void paint(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (j == fila_salida && i == columna_salida) {
                    g.drawImage(puerta_salida, j * 40, i * 40, 40, 40, this);
                } else {
                    if (i == columna && j == fila) {
                        g.drawImage(personaje, j * 40, i * 40, 40, 40, this);
                    } else {
                        if (matriz[i][j] == 1) {
                            g.drawImage(muro, j * 40, i * 40, 40, 40, this);
                        } else {
                            if (matriz[i][j] == 0) {
                                g.setColor(Color.GREEN);
                                g.fillRect(j * 40, i * 40, 40, 40);
                                g.setColor(Color.BLUE);
                                g.drawRect(j * 40, i * 40, 40, 40);
                            }else{
                               g.drawImage(flama, j * 40, i * 40, 40, 40, this); 
                            }
                        }
                    }
                }
            }
        }
    }

    public void abajo() {
        if (!resuelto && columna + 1 < matriz.length && matriz[columna + 1][fila] != 1) {
            System.out.println(columna + "-" + matriz.length);
            columna++;
            resuelto();
            if (resuelto) {
                JOptionPane.showMessageDialog(this, "Felicidades has ganado");
            } else {
                repaint();
            }

        }
    }

    public void arriba() {
        System.out.println("intento moverme hacia arriba");
        if (!resuelto && columna - 1 >= 0 && matriz[columna - 1][fila] != 1) {
            System.out.println(columna + "-" + matriz.length);
            columna--;
            resuelto();
            if (resuelto) {
                JOptionPane.showMessageDialog(this, "Felicidades has ganado");
            } else {
                repaint();
            }
        }
    }

    public void derecha() {
        System.out.println("intento moverme hacia derecha" + fila);
        
        if (!resuelto && fila + 1 < matriz.length && matriz[columna][fila + 1] != 1) {
            System.out.println(fila + "-" + matriz.length);
            fila++;
            resuelto();
            if (resuelto) {
                JOptionPane.showMessageDialog(this, "Felicidades has ganado");
            } else {
                repaint();
            }
        }
    }

    public void izquierda() {
        System.out.println("intento moverme hacia izquierda" + fila);
        
        if (!resuelto && fila - 1 >= 0 && matriz[columna][fila - 1] != 1) {
            System.out.println(fila + "-" + matriz.length);
            fila--;
            resuelto();
            if (resuelto) {
                JOptionPane.showMessageDialog(this, "Felicidades has ganado");
            } else {
                repaint();
            }
        }
    }

    private void resuelto() {
        Random r = new Random();
        matriz[i][j] = aux;
        j = r.nextInt(matriz.length);
        r = new Random();
        i = r.nextInt(matriz.length);
        aux = matriz[i][j];
        matriz[i][j] = 2;
        if (columna == columna_salida && fila_salida == fila) {
            resuelto = true;
        }else{
            if (matriz[columna][fila]==2) {
                 JOptionPane.showMessageDialog(this,"HAS MUERTO QUEMADO POR UNA FLAMA");
                 matriz = new int[][]{
            {0, 1, 2, 1, 0, 0},
            {0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 0, 0},
            {0, 1, 1, 0, 1, 0},
            {0, 1, 1, 0, 1, 0},
            {0, 1, 1, 1, 0, 0}
        };
        fila = 0;
        columna = 0;
        fila_salida = 1;
        columna_salida = 3;
        resuelto = false;
        matriz[columna_salida][fila_salida] = 0;
        repaint();
            }
        }
        setFocusable(true);
    }

    public static void main(String[] args) {
       
        Escenario e = new Escenario();
        JFrame ventana = new JFrame();
        ventana.add(e);
        e.derecha();
        e.abajo();
        e.izquierda();
        e.derecha();
        e.izquierda();
        e.derecha();
        e.izquierda();
        e.derecha();
        e.abajo();
        ventana.setVisible(true);
        ventana.setSize(400, 400);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
